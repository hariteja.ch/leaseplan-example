Feature: Search for the product car with invalid endpoints

Scenario: Validate no results displayed for car with invalid endpoint
When he calls invalid car endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/appl"
Then he sees the message not found in response for invalid car endpoint