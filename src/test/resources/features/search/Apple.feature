Feature: Search for the product apple with valid and invalid endpoints

Scenario: Validate results displayed for apple with valid endpoint
When he calls valid apple endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/apple"
Then he sees the valid results displayed for apple

Scenario: Validate no results displayed for apple with invalid endpoint
When he calls invalid apple endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/appl"
Then he sees the message not found in response for invalid apple endpoint