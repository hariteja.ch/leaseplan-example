Feature: Search for the product mango with valid and invalid endpoints

Scenario: Validate results displayed for mango when we call a valid endpoint
When he calls valid mango endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/mango"
Then he sees the results displayed for mango

Scenario: Validate no results displayed for mango when we call a invalid endpoint
When he calls invalid mango endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/man"
Then he sees the message not found in response