package starter.stepdefinitions;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;

public class apple {

	@Steps
    public CarsAPI carsAPI;

    @When("^he calls valid apple endpoint \"(.*)\"$")
    public void heCallsEndpoint(String endpoint) {
        SerenityRest.given().get(endpoint);
    }

    @Then("^he sees the valid results displayed for apple$")
    public void heSeesTheResultsDisplayedForApple() {
        restAssuredThat(response -> response.statusCode(200));
    }
    
    @When("^he calls invalid apple endpoint \"(.*)\"$")
    public void heCallsInvalidEndpoint(String endpoint) {
        SerenityRest.given().get(endpoint);
    }

    @Then("^he sees the message not found in response for invalid apple endpoint$")
    public void heSeesTheResultsDisplayedForMango() {
    	SerenityRest.restAssuredThat(response -> response.statusCode(404));
//    	restAssuredThat(response -> response.body("data.employee_name", equal("")));
    }
}
