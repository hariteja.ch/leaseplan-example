package starter.stepdefinitions;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;

public class mango {
	
	@When("^he calls valid mango endpoint \"(.*)\"$")
    public void heCallsEndpoint(String endpoint) {
        SerenityRest.given().get(endpoint);
    }

    @Then("he sees the results displayed for mango")
    public void heSeesTheResultsDisplayedForMango() {
        restAssuredThat(response -> response.statusCode(200));
    }
    
    @When("^he calls invalid mango endpoint \"(.*)\"$")
    public void heCallsInvalidEndpoint(String endpoint) {
        SerenityRest.given().get(endpoint);
    }

    @Then("^he sees the message not found in response$")
    public void heSeesTheErrorResultsDisplayedForMango() {
    	SerenityRest.restAssuredThat(response -> response.statusCode(404));
//    	restAssuredThat(response -> response.body("data.employee_name", equal("")));
    }

}
