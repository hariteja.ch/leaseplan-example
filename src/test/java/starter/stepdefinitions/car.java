package starter.stepdefinitions;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;

public class car {
	
	@When("^he calls invalid car endpoint \"(.*)\"$")
    public void heCallsInvalidCarEndpoint(String endpoint) {
        SerenityRest.given().get(endpoint);
    }

    @Then("he sees the message not found in response for invalid car endpoint")
    public void heSeesTheResultsNotDisplayedForCar() {
        restAssuredThat(response -> response.statusCode(404));
    }
}
