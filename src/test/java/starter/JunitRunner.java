package starter;

import org.junit.runner.RunWith;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(plugin = { "pretty", "json:target/cucumber-reports/Cucumber.json",
		"html:target/cucumber-report/cucumber.html"

}, features = "src/test/resources/features/search", glue = { "starter.stepdefinitions" }

)

public class JunitRunner {

}
